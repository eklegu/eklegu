var prefixLists = [];
var completeLists = [];
var number_of_lines = 0;
var prefixJsonStack;
var rootJson;
var suffixJsonStack;
var endingJson;
var SIMPLE_ENDINGS = ["o", "i", "a", "e"];
var PARTICIPIAL_ENDINGS = ["ant", "at", "int", "it", "ont", "ot"];
var POSSIBLE_PREFIXES = null; // loaded in background.js init(); 
var POSSIBLE_SUFFIXES = null; // loaded in background.js init(); 

var originalSimpleEnding;

function makeNewPrefixList(oldList, prefix, word) {
    var oldPrefixes = oldList[0];
    var newPrefixes = [];
    for (var i = 0; i < oldPrefixes.length; i++) {
        newPrefixes.push(oldPrefixes[i]);
    }
    newPrefixes.push(prefix);
    return [newPrefixes, word, []];
}

function makeNewSuffixList(oldList, suffix, word) {
    var prefixes = oldList[0];
    var oldSuffixes = oldList[2];
    var newSuffixes = [suffix];
    for (var i = 0; i < oldSuffixes.length; i++) {
        newSuffixes.push(oldSuffixes[i]);
    }
    return [prefixes, word, newSuffixes];
}

function findPrefixes(currentPrefixList) {
    var prefixedWord = currentPrefixList[1];
    for (var i = 0; i < POSSIBLE_PREFIXES.length; i++) {
        var prefix = POSSIBLE_PREFIXES[i];
        if (prefix > prefixedWord) {
            return;
        }
        if (prefixedWord.startsWith(prefix)) {
            // strip prefix from word
            var wordWithoutPrefix = prefixedWord.substring(prefix.length, prefixedWord.length);
            if (wordWithoutPrefix.length > 1) {
                var newPrefixList = makeNewPrefixList(
                        currentPrefixList, prefix, wordWithoutPrefix);
                prefixLists.push(newPrefixList);
                findPrefixes(newPrefixList);
            }
        }
    }
}

function findSuffixes(currentSuffixList) {
    var suffixedWord = currentSuffixList[1];
    for (var i = 0; i < POSSIBLE_SUFFIXES.length; i++) {
        var suffix = POSSIBLE_SUFFIXES[i];
        if (suffixedWord.endsWith(suffix)) {
            // strip suffix from word
            var wordWithoutSuffixLength = suffixedWord.length - suffix.length;
            var wordWithoutSuffix = suffixedWord.substring(0, wordWithoutSuffixLength);
            if (wordWithoutSuffix.length > 1) {
                var newSuffixList = makeNewSuffixList(
                        currentSuffixList, suffix, wordWithoutSuffix);
                completeLists.push(newSuffixList);
                findSuffixes(newSuffixList);
            }
        }
    }
}

function getJson(term, meaning, main) {
    var definition = {};
    definition.term = term;
    number_of_lines++;
    if (main) {
        definition.main = meaning;       
    } else {
        definition.text = meaning;
    }
    number_of_lines += Math.ceil((meaning.length / 40));
    return definition;
}

function processAffixes(affixList, affixDefs) {
    var affixJsonStack = [];
    for (var i = 0; i < affixList.length; i++) {
        var affix = affixList[i];
        affixJsonStack.push( getJson(affix, affixDefs[affix], false) );
    }
    return affixJsonStack;
}

function inDictionary(word) {
    var definition = espdic[word];
    if (!definition || definition.length === 0) {
        return false;
    }
    rootJson = getJson(word, definition, true);
    return word;
}

function testEntry(word, prefixes, suffixes) {
    if (inDictionary(word)) {
        prefixJsonStack = processAffixes(prefixes, prefixDefs);
        suffixJsonStack = processAffixes(suffixes, suffixDefs);
        return true;
    }
    return false;
}

function isParticipial(suffixes) {
    for (var i = 0; i < suffixes.length; i++) {
        var suffix = suffixes[i];
        for (var j = 0; j < PARTICIPIAL_ENDINGS.length; j++) {
            if (suffix === PARTICIPIAL_ENDINGS[j]) {
                return true;
            }
        }
    }
    return false;
}

function processCompleteLists() {
    // Sort by word size, largest words first...
    completeLists.sort(function (a, b) {
        // ASC  -> a.length - b.length
        // DESC -> b.length - a.length
        return b[1].length - a[1].length;
    });
    // Now process the first list whose root is in dictionary and return true...
    for (var i = 0; i < completeLists.length; i++) {
        var simpleEnding = originalSimpleEnding;
        var partsList = completeLists[i];
        var rootWord = partsList[1];
        var prefixes = partsList[0];
        var suffixes = partsList[2];
        if (isParticipial(suffixes)) {
            simpleEnding = "i";
        }
        if (testEntry(rootWord + simpleEnding, prefixes, suffixes)) {
            return true;
        }
        // try other simple endings
        for (var j = 0; j < SIMPLE_ENDINGS.length; j++) {
            if (!(SIMPLE_ENDINGS[j] === simpleEnding)) {
                if (testEntry(rootWord + SIMPLE_ENDINGS[j], prefixes, suffixes)) {
                    return true;
                }
            }
        }
        // try with no ending
        if (testEntry(rootWord, prefixes, suffixes)) {
            return true;
        }
    }
    // Not found...
    return false;
}

function sliceAndDice(word) {
    findPrefixes([[], word, []]);
    completeLists.push([[], word, []]);
    for (var i = 0; i < prefixLists.length; i++) {
        var prefixList = prefixLists[i];
        completeLists.push(prefixList);
        findSuffixes(prefixList);
    }
    findSuffixes([[], word, []]);
    if (processCompleteLists()) {
        return true;
    }
    return false;
}

function hasEnding(wordWithEnding, endings) {
    for (var i = 0; i < endings.length; i++) {
        if (wordWithEnding.endsWith(endings[i])) {
            endingJson = getJson(endings[i], endingDefs[endings[i]], false);
            // return the word without the ending
            var lengthOfRoot = wordWithEnding.length - endings[i].length;
            return wordWithEnding.substring(0, lengthOfRoot);
        }
    }
    return false;
}

function checkForEnding(word) {
    var wordWithoutEnding = false;
    if ((wordWithoutEnding = hasEnding(word, ["o", "oj", "on", "ojn"]))) {
        originalSimpleEnding = "o";
    } else if ((wordWithoutEnding = hasEnding(word, ["as", "is", "os", "u", "us", "i"]))) {
        originalSimpleEnding = "i";
    } else if ((wordWithoutEnding = hasEnding(word, ["a", "aj", "an", "ajn"]))) {
        originalSimpleEnding = "a";
    } else if ((wordWithoutEnding = hasEnding(word, ["e", "en"]))) {
        originalSimpleEnding = "e";
    }
    return wordWithoutEnding;
}

function search(rawWord) {
    // reset both Json stack and prefix Json stack
    number_of_lines = 0;
    prefixLists = [];
    completeLists = [];
    prefixJsonStack = null;
    rootJson = null;
    suffixJsonStack = null;
    originalSimpleEnding = null;
    endingJson = null;
    // Lookup word as is. This will word for simple words like prepositions, articles and most adverbs.
    var found = inDictionary(rawWord);
    if (!found) {
        // Check for endings. Replace with simple ending for part of speech.
        var wordWithoutEnding = checkForEnding(rawWord);
        if (wordWithoutEnding) {
            if (!(found = inDictionary(wordWithoutEnding + originalSimpleEnding))) {
                found = sliceAndDice(wordWithoutEnding);
            }
        } else { // (it may be the first half of a hyphenated or  agglutinated word)
            // try a noun ending
            if (!(found = inDictionary(rawWord + "o"))) {
                // try adjective ending
                if (!(found = inDictionary(rawWord + "a"))) {
                    found = sliceAndDice(rawWord);
                }
            }
        }
    }
    if (found) {
        var response = 
        {
            "definitionList" : [], 
            "height" : (30 + (number_of_lines * 15))
        };
        if (prefixJsonStack && (prefixJsonStack.length > 0)) {
            response.definitionList.push.apply(response.definitionList, prefixJsonStack);
        }
        if (rootJson) {
            response.definitionList.push(rootJson);
        }
        if (suffixJsonStack && (suffixJsonStack.length > 0)) {
            response.definitionList.push.apply(response.definitionList, suffixJsonStack);
        }
        if (endingJson) {
            response.definitionList.push(endingJson);
        }
        return response;
    }
    return false;
}

