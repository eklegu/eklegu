Copyright (C) 2016, 2017 Gregory Smith

Licenses for Eklegu!

Eklegu! was written by Gregory Smith (a.k.a. Gregorio Forĝisto) in November 2016 and is licensed under the GNU General Public License 2.0:

https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt

It uses the ESPDIC (Esperanto English Dictionary) by Paul Denisowski which is licensed under a Creative Commons Attribution 3.0 Unported License:

http://www.denisowski.org/Esperanto/espdic_readme.htm#overview
https://creativecommons.org/licenses/by/3.0/
