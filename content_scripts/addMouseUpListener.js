function appendEkleguPopupStyle() {
    var lightBoxStyle = "#EkleguLightBox {display:none; background:#fff; opacity:1; position:fixed; top:10%; left:5%; width:290px; height:60px; z-index:1000; border:2px solid #000;margin: 0px; padding:10px; padding-top:5px;}";
    var dlStyle = "#EkleguLightBox > dl {margin: 0px; padding:0px;}";
    var dtStyle = "#EkleguLightBox > dl > dt {margin: 0px; padding:0px; padding-top:5px; font-family:'Times New Roman',serif; font-size:14px; color:black; line-height: 1.0; font-weight: bold; text-decoration: none; background: transparent;}";
    var ddStyle = "#EkleguLightBox > dl > dd {margin: 0px; padding:0px; padding-top:2px; font-family:'Times New Roman',serif; font-size:14px; color:black; line-height: 1.0; font-weight: normal; text-decoration: none; background: transparent;}";
    var css = document.createElement('style');
    css.type = 'text/css';
    // css.innerHTML = styles;
    if (css.styleSheet) {
        css.styleSheet.cssText = styles;
    } else {
        css.appendChild(document.createTextNode(lightBoxStyle));
        css.appendChild(document.createTextNode(dlStyle));
        css.appendChild(document.createTextNode(dtStyle));
        css.appendChild(document.createTextNode(ddStyle));
    }
    document.body.appendChild(css);
}

function createEkleguLightBox() {
    var lightBox = document.createElement("div");
    lightBox.setAttribute("id", "EkleguLightBox");
    document.body.appendChild(lightBox);
}

function showEkleguLightBox(response) {
    if (response) {
        var lightBoxDiv = document.getElementById('EkleguLightBox');
        while (lightBoxDiv.hasChildNodes()) {
            lightBoxDiv.removeChild(lightBoxDiv.lastChild);
        }
        lightBoxDiv.style.display = 'block';
        lightBoxDiv.style.height = "" + response.height + "px";
        var dl = document.createElement("dl");
        //var defintionList = [{term: "word", text: "This is some kind of definition here."}, {term: "ending", text: "Ending definition."}];
        var defintionList = response.definitionList;
        for (var i = 0; i < defintionList.length; i++) {
            var definition = defintionList[i];
            var dt = document.createElement("dt");
            var dtTerm = document.createTextNode(definition.term);
            dt.appendChild(dtTerm);
            var dd = document.createElement("dd");
            var ddContent = null;
            if (definition.main) {
                ddContent = document.createTextNode(definition.main);
            } else {
                ddContent = document.createElement("I");
                var ddText = null;
                if (definition.text) {
                    ddText = document.createTextNode(definition.text);
                
                } else {
                    ddText = document.createTextNode("Not found");
                }
                ddContent.appendChild(ddText);
            }
            dd.appendChild(ddContent);
            dl.appendChild(dt);
            dl.appendChild(dd);
        }
        lightBoxDiv.appendChild(dl);
    } else {
        closeEkleguLightBox();
    }
}

function closeEkleguLightBox() {
    document.getElementById('EkleguLightBox').style.display = 'none';
}

function elektuVorton() {
    var selection = window.getSelection().toString();
    if (!selection || ((selection.trim() === "") || selection.length > 40)) {
        closeEkleguLightBox();
    } else {
        chrome.runtime.sendMessage({selection: selection}, showEkleguLightBox);
    }
}

//window.onload = function() { 
appendEkleguPopupStyle();
createEkleguLightBox();
document.addEventListener('mouseup', elektuVorton, false);
//};
