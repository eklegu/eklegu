'use strict';

// Remove console.log statements in production. (Comment out for debugging.)
//var console = {};
//console.log = function(){};

var active = false;

// JSON vortaro objects created in init()
var endingDefs = null;
var espdic = null;
var prefixDefs = null;
var suffixDefs = null;

function onDeactivateContentScript(results) {
    console.log("Deactivated content script: " + results);
}

function onActivateContentScript(results) {
    console.log("Activated content script: " + results);
}

function handleXs(word) {
    if (/x|X/.test(word)) {
        word = word.replace(/Cx|CX/g, "Ĉ");
        word = word.replace(/cx/g, "ĉ");
        word = word.replace(/Gx|GX/g, "Ĝ");
        word = word.replace(/gx/g, "ĝ");
        word = word.replace(/Hx|HX/g, "Ĥ");
        word = word.replace(/hx/g, "ĥ");
        word = word.replace(/Jx|JX/g, "Ĵ");
        word = word.replace(/jx/g, "ĵ");
        word = word.replace(/Sx|SX/g, "Ŝ");
        word = word.replace(/sx/g, "ŝ");
        word = word.replace(/Ux|UX/g, "Ŭ");
        word = word.replace(/ux/g, "ŭ");
    }
    return word;
}

function handleELibroPDF(word) {
    if (/ç| ̧|̧| ̨|̨|î|ÿ|û|Ç| ̄|̄| ̆|̆|Î|Ÿ|Û/.test(word)) {
        word = word.replace(/Ç/g, "Ĉ");
        word = word.replace(/ç/g, "ĉ");
        word = word.replace(/ ̄|̄/g, "Ĝ");
        word = word.replace(/ ̧|̧/g, "ĝ");
        word = word.replace(/ ̆|̆/g, "Ĥ");
        word = word.replace(/ ̨|̨/g, "ĥ");
        word = word.replace(/Î/g, "Ĵ");
        word = word.replace(/î/g, "ĵ");
        word = word.replace(/Ÿ/g, "Ŝ");
        word = word.replace(/ÿ/g, "ŝ");
        word = word.replace(/Û/g, "Ŭ");
        word = word.replace(/û/g, "ŭ");
    }
    return word;
}

function handleHyphenNewLine(word) {
    if (/-[\s]?\n/.test(word)) {
        var amalgam = "";
        var fragments = word.split(/-[\s]?\n/);
        for (var i = 0; i < fragments.length; i++) {
            amalgam = amalgam + fragments[i];
        }
        word = amalgam;
    }
    return word;
}

function handleSelection(word) {
    console.log("selection:" + word);
    var response = null;
    if ((word.length === 0) || (word.length > 40)) {
        // this shouldn't happen, but just in case...
        response = null; // forces lightbox to close.
    } else { // good to go
        word = handleXs(word);
        word = handleELibroPDF(word);
        word = handleHyphenNewLine(word);
        response = search(word);
        if (!response) {
            // Check for upper case in word...
            var lowerCaseWord = word.toLowerCase();
            if (lowerCaseWord !== word) {
                // try again with all lower case
                response = search(lowerCaseWord);
            }
            if (!response) { // if still no definition
                response = {
                    definitionList: [{
                        "term": word,
                        "text": null
                    }],
                    "height": 60
                };
            }
        }
    }
    return response;
}

function listenForMessage(request, sender, sendResponse) {
    console.log("from a content script:" + sender.tab.url);
    if (active) {
        if (request.selection) { // this is a selection from mouseUp listener
            var response = handleSelection(request.selection.trim());
            sendResponse(response);
        } else {
            console.log("Received unknown request.");
        }
    } else {
        console.log("Word Search is currently inactive.");
    }
}

function clickExtensionIcon(aTab) {
    if (active) {
        // Deactivate!
        chrome.browserAction.setBadgeBackgroundColor({color: '#999'});
        active = false;
    } else {
        // Activate!
        chrome.browserAction.setBadgeBackgroundColor({color: '#0f0'});
        active = true;
    }
}

function loadJsonFile(file, callback) {
    var xhr = new XMLHttpRequest();
    xhr.overrideMimeType("application/json");
    xhr.responseType = "json";
    xhr.open('GET', file, true);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            callback(xhr.response);
        }
    }
    xhr.send();
}

function init() {
    loadJsonFile("/vortaro/endingDefs.json", function (jsonObj) {
        endingDefs = jsonObj;
    });
    loadJsonFile("../vortaro/espdic.json", function (jsonObj) {
        espdic = jsonObj;
    });
    loadJsonFile("vortaro/prefixDefs.json", function (jsonObj) {
        prefixDefs = jsonObj;
        POSSIBLE_PREFIXES = Object.keys(prefixDefs);
    });
    loadJsonFile("vortaro/suffixDefs.json", function (jsonObj) {
        suffixDefs = jsonObj;
        POSSIBLE_SUFFIXES = Object.keys(suffixDefs);
    });
    chrome.browserAction.setBadgeText({text: '?'});
    chrome.browserAction.setBadgeBackgroundColor({color: '#999'});
    chrome.runtime.onMessage.addListener(listenForMessage);
    chrome.browserAction.onClicked.addListener(clickExtensionIcon);
// We always add mouseUpListener to any new page that is loaded.
    chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
        if (changeInfo.status == 'complete') {
            chrome.tabs.executeScript(tabId,
                    {
                        allFrames: true,
                        file: "/content_scripts/addMouseUpListener.js"
                    },
                    onActivateContentScript
                    );
        }
    });
}

init();
